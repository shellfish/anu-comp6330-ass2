#define AVR_IMPL
#include "avr.h"
#include <libusb-1.0/libusb.h>
#include <stdlib.h>
#include <stdio.h>
#define INTERFACE_NO 0
#define ERRCODE(code) ERRMSG(libusb_error_name(code))


enum Status {
    STAT_START = 0,
    STAT_INITED = 1,
    STAT_DEVICE_OPENED,
    STAT_INTERFACE_CLAIMED,
    STAT_LAST,
};

enum Status global_stat;
libusb_device_handle *global_dev;

int send_data(enum Request req, uint16_t value, unsigned char*buf, size_t len)
{
    return libusb_control_transfer(
        global_dev,
        LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_ENDPOINT_OUT,
        req,   // request
        value,  // value
        0,   // index
        buf, // buffer
        len, // length
        0 // timeout
    );
}

int send_packet(enum Request req)
{
    return send_data(req, 0, 0, 0);
}

int recv_data(enum Request req, uint16_t value, unsigned char *buf, size_t len)
{
    return libusb_control_transfer(
        global_dev,
        LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_ENDPOINT_IN,
        req,   // request
        value,  // value
        0,   // index
        buf, // buffer
        len, // length
        0 // timeout
    );
}

__attribute__((noreturn)) void cleanup_and_exit()
{
    if (global_stat >= STAT_INTERFACE_CLAIMED)
        libusb_release_interface(global_dev, INTERFACE_NO);
    if (global_stat >= STAT_DEVICE_OPENED)
        libusb_close(global_dev);
    if (global_stat >= STAT_INITED)
        libusb_exit(NULL);
    exit(global_retval);
}

void setup_usb(int nousb)
{
    if (nousb)
        return;
    int r;
    if ((r = libusb_init(NULL)) != 0) {
        ERRCODE(r);
        cleanup_and_exit();
    } else
        global_stat = STAT_INITED;

    libusb_set_debug(NULL, 3);
    global_dev = libusb_open_device_with_vid_pid(NULL, AVR_VENDOR_ID, AVR_PRODUCT_ID);
    if (!global_dev) {
        ERRMSG("cannot open device");
        cleanup_and_exit();
    } else
        global_stat = STAT_DEVICE_OPENED;

    if ((r = libusb_claim_interface(global_dev, INTERFACE_NO)) != 0) {
        ERRCODE(r);
        cleanup_and_exit();
    } else
        global_stat = STAT_INTERFACE_CLAIMED;
}

void get_avr_status(unsigned char (*buf)[2])
{
    int r;
    if ((r = recv_data(REQ_STATUS, 0, *buf, sizeof(*buf))) != sizeof(*buf)) {
        ERRCODE(r);
        cleanup_and_exit();
    }
}

int action_status()
{
    unsigned char buf[2];
    get_avr_status(&buf);
    printf(STAT_FORMAT, buf[0], buf[1]);
    return EXIT_SUCCESS;
}


int action_download_key()
{
    int r;
    unsigned char buf[16];
    if ((r = recv_data(REQ_CHANGE_KEY, 0, buf, sizeof(buf))) != sizeof(buf)) {
        ERRCODE(r);
        return EXIT_FAILURE;
    } else {
        for (int i=0; i < 16; i++)
            printf("%02x", buf[i]);
        printf("\n");
        return EXIT_SUCCESS;
    }
}


int send_ctl_packet(enum Request req)
{
    int r;
    if ((r = send_packet(req)) != 0) {
        ERRCODE(r);
        return EXIT_FAILURE;
    } else
        return EXIT_SUCCESS;
}

int action_led_on()
{
    return send_ctl_packet(REQ_LED_ON);
}

int action_led_off()
{
    return send_ctl_packet(REQ_LED_OFF);
}


int led_flush_ctl(int on)
{
    int r;
    if ((r = send_data(REQ_LED_CTL, on, 0, 0)) != 0) {
        ERRCODE(r);
        return EXIT_FAILURE;
    } else
        return EXIT_SUCCESS;
}

int action_led_flush_on()
{
    return led_flush_ctl(1);
}

int action_led_flush_off()
{
    return led_flush_ctl(0);
}

void emit_block(unsigned char (*buf)[16])
{
    int block_index = 0;
    int r;
    do {
        uint16_t *data = (uint16_t*)(*buf + block_index * 4);
        r = libusb_control_transfer(
            global_dev,
            LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_ENDPOINT_OUT,
            REQ_UPLOAD_A + block_index,   // request
            *data,  // value
            *(data+1),   // index
            0, // buffer
            0, // length
            0 // timeout
        );
        if (r != 0) {
            ERRMSG("io error when emitting plain text");
            cleanup_and_exit();
        }
    } while (++block_index != 4);
}

void recv_block(unsigned char (*buf)[16])
{
    int r;
    if ((r = recv_data(REQ_DOWNLOAD_A, 0, *buf, 8)) != 8)
        goto err;
    if ((r = recv_data(REQ_DOWNLOAD_B, 0, (*buf) + 8, 8)) != 8)
        goto err;
    return;
err:
    ERRMSG("io error when receiving encrypted data block");
    cleanup_and_exit();
}

void signal_encryption()
{
    int r;
    if ((r = send_packet(REQ_START_ENCRYPT)) != 0) {
        ERRCODE(r);
        cleanup_and_exit();
    }
}

void block_when_encrypting()
{
    unsigned char stat[2] = {0};
    do {
        get_avr_status(&stat);
    } while (stat[0]);
}

void encrypt_and_recv_block(unsigned char (*buf)[16])
{
    emit_block(buf);
    signal_encryption();
    block_when_encrypting();
    recv_block(buf);
}

int write_key(unsigned char *hexbuf)
{
    int r = send_data(REQ_CHANGE_KEY, 0, hexbuf, 16);
    if (r != 16) {
        ERRCODE(r);
        return EXIT_FAILURE;
    } else
        return EXIT_SUCCESS;
}
