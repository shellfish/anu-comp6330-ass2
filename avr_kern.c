#define AVR_IMPL
#include "avr.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

extern int global_retval;

// null operation

__attribute__((noreturn)) void cleanup_and_exit()
{
    exit(global_retval);
}

const char *cat(const char *part1, ...)
{
    static char buf[2048];
    char *p = buf;
    const char *part;
    va_list ap;

    p[0] = 0;
    p = strcat(p, part1);
    va_start(ap, part1);
    while ((part = va_arg(ap, const char *)))
         p = strcat(p, part);
    va_end(ap);
    return p;
}
#define CAT(a, ...) cat(a, __VA_ARGS__, NULL)
#define SYSTEM(a, ...) system(CAT(a, __VA_ARGS__))

void setup_usb(int _) { }

int action_status()
{
    return SYSTEM("cat /sys/class/crypt/avrkrypt", device_index, "/stat");
}

int action_download_key()
{
    return SYSTEM("cat /sys/class/crypt/avrkrypt", device_index, "/key | xxd -p");
}


int action_led_on()
{
    return SYSTEM("echo 1 > /sys/class/crypt/avrkrypt", device_index, "/led");
}

int action_led_off()
{
    return SYSTEM("echo 0 > /sys/class/crypt/avrkrypt", device_index, "/led");
}


int action_led_flush_on()
{
    return SYSTEM("echo 1 > /sys/class/crypt/avrkrypt", device_index, "/flash");
}

int action_led_flush_off()
{
    return SYSTEM("echo 0 > /sys/class/crypt/avrkrypt", device_index, "/flash");
}

void encrypt_and_recv_block(unsigned char (*buf)[16])
{
    static int fd = 0;
    if (!fd)
        fd = open(CAT("/dev/crypt/avrkrypt", device_index), O_RDWR);

    if (fd == -1) {
        fprintf(stderr, "cannot open encryption device\n");
        exit(1);
    }
    int r;
    if (16 != (r = write(fd, *buf, 16))) {
        fprintf(stderr, "write %d\n", r);
        perror("failed to upload data");
        exit(2);
    }
    if (16 != (r = read(fd, *buf, 16))) {
        fprintf(stderr, "read %d\n", r);
        perror("failed to download data");
        exit(2);
    }
}

int write_key(unsigned char *hexbuf)
{
    int fd = open(CAT("/sys/class/crypt/avrkrypt", device_index, "/key"), O_WRONLY);
    if (fd == -1) {
        fprintf(stderr, "cannot open key file\n");
        return 1;
    }
    if (write(fd, hexbuf, 16) != 16) {
        perror("update key:");
        return 2;
    }
    return 0;
}

