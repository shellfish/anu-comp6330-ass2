#include "../avrk.h"
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/version.h>
#include <linux/version.h>

extern const struct device_attribute **dev_attributes;

struct PrivateData {
    struct cdev cdev;
    struct usb_interface *uintf;
    dev_t devno;
    struct device *device;
    struct AvrkDev *self;
};

static dev_t get_devno(struct AvrkDev* dev)
{
    struct PrivateData *pridata = (struct PrivateData*)dev->private_data;
    return pridata->devno;
}

static struct AvrkDev *new(struct usb_interface *intf)
{
    struct AvrkDev *dev = NULL;
    int retval = -ENOMEM;

    dev = kzalloc(sizeof(*dev), GFP_KERNEL);
    if (!dev) {
        pr_err("Out of memory");
        goto memerr1;
    } else
        dev->class = &AvrkDevClass;

    struct PrivateData *pridata = kmalloc(sizeof(*pridata), GFP_KERNEL);
    if (!pridata)
        goto memerr2;
    else {
        dev->private_data = pridata;
        pridata->self = dev;
    }

    kref_init(&dev->kref);
    mutex_init(&dev->lock);
    cdev_init(&pridata->cdev, &avrk_fops);

    dev->udev = usb_get_dev(interface_to_usbdev(intf));
    pridata->uintf = intf;
    usb_set_intfdata(intf, dev);
    int minor = CALL(DevNumber, alloc_minor);
    if (minor == -1) {
        pr_err("no minor number available");
        retval = -EBUSY;
        goto usb_alloc_minor_err;
    }

    pridata->devno = CALL(DevNumber, mkdev, minor);
    retval = cdev_add(&pridata->cdev, pridata->devno, 1);
    if (retval) {
        pr_err("cannot add char dev");
        goto devno_alloc_err;
    }


    CALL(CryptClass, ref);

    pridata->device = device_create(GET(CryptClass),
            NULL, pridata->devno, dev,
            "avrkrypt%d", minor);
    if (IS_ERR(pridata->device)) {
        retval = PTR_ERR(pridata->device);
        pr_err("cannot register avrkcrypt device in /sys");
        goto cdev_add_err;
    }

    const struct device_attribute **ap = dev_attributes;
    while (*ap) {
        if ((retval = device_create_file(pridata->device, *ap))) {
            ap -= 1;
            goto device_create_file_err;
        } else
            ap += 1;
    }

    return dev;

device_create_file_err:
    while (ap >= dev_attributes) {
        device_remove_file(pridata->device, *ap);
        ap -= 1;
    }
    cdev_del(&pridata->cdev);
devno_alloc_err:
    CALL(DevNumber, free_minor, minor);
cdev_add_err:
    CALL(CryptClass, unref);
    cdev_del(&pridata->cdev);
usb_alloc_minor_err:
    usb_set_intfdata(intf, NULL);
    PCALL(dev, unref);
    kfree(dev->private_data);
memerr2:
    kfree(dev);
memerr1:
    return ERR_PTR(retval);
}

static void unref_helper(struct kref *kref)
{
    struct AvrkDev *dev = container_of(kref, struct AvrkDev, kref);
    usb_put_dev(dev->udev);
    kfree(dev);
}

static int lock(struct AvrkDev *dev)
{
    return mutex_lock_interruptible(&dev->lock);
}

static void unlock(struct AvrkDev *dev)
{
    mutex_unlock(&dev->lock);
}

static void delete(struct AvrkDev *dev)
{
    struct PrivateData *pridata = (struct PrivateData*) dev->private_data;

#ifndef HAS_NEW_SYSFS_API
    const struct device_attribute **ap = dev_attributes;
    while (*ap)
        device_remove_file(pridata->device, *ap++);
#endif

    /* remove /sys entry */
    device_destroy(GET(CryptClass), pridata->devno);
    CALL(CryptClass, unref);

    /* delete char dev */
    cdev_del(&pridata->cdev);

    /* detach usb driver */
    usb_set_intfdata(pridata->uintf, NULL);

    /* delete usb dev */
    CALL(DevNumber, free_minor, MINOR(pridata->devno));

    kfree(pridata);
    dev->private_data = NULL;

    PCALL(dev, unref);
}

static void ref(struct AvrkDev *dev)
{
    kref_get(&dev->kref);
}

static void unref(struct AvrkDev *dev)
{
    kref_put(&dev->kref, unref_helper);
}

static int sendex(struct AvrkDev* dev, uint8_t request,
                  uint16_t value, uint16_t index,
                  const void *data, uint16_t size)
{
    return usb_control_msg(dev->udev,
        usb_sndctrlpipe(dev->udev, 0), // control pipe
        request, // request 2 -> led on
        USB_TYPE_VENDOR | USB_DIR_OUT,
        value,
        index,
        (void*)data,
        size,
        USB_CTRL_SET_TIMEOUT // default timeout
    );
}

static int send(struct AvrkDev* dev, uint8_t request)
{
    return PCALL(dev, sendex, request, 0, 0, NULL, 0);
}

static int recvex(struct AvrkDev* dev, uint8_t request,
                  uint16_t value, uint16_t index,
                  void *data, uint16_t size)
{
    return usb_control_msg(dev->udev,
        usb_rcvctrlpipe(dev->udev, 0), // control pipe
        request, // request 2 -> led on
        USB_TYPE_VENDOR | USB_DIR_IN,
        value,
        index,
        data,
        size,
        USB_CTRL_SET_TIMEOUT // default timeout
    );
}

int upload(struct AvrkDev* dev, int idx, uint8_t *block4b)
{
    if (idx < 0 && idx > 3)
        return -EINVAL;
    uint16_t value;
    uint16_t index;
    memcpy(&value, block4b, 2);
    memcpy(&index, block4b + 2, 2);
    return PCALL(dev, sendex, idx + REQ_UPLOAD_A, value, index, NULL, 0);
}

int download(struct AvrkDev* dev, int idx, uint8_t *block8b)
{
    if (idx < 0 && idx > 1)
        return -EINVAL;
    return PCALL(dev, recvex, idx + REQ_DOWNLOAD_A, 0, 0, block8b, 8);
}

int start(struct AvrkDev* dev)
{
    return PCALL(dev, send, REQ_START_ENCRYPT);
}

static int recv(struct AvrkDev* dev, uint8_t request)
{
    return PCALL(dev, recvex, request, 0, 0, NULL, 0);
}

static int ledctl(struct AvrkDev *dev, int dest)
{
    return PCALL(dev, send, dest ? REQ_LED_ON : REQ_LED_OFF);
}

static int flashctl(struct AvrkDev *dev, int dest)
{
    return PCALL(dev, sendex, REQ_LED_CTL, dest && 0x0101, 0, 0, 0);
}

static int stat(struct AvrkDev *dev, char *stat)
{
    int r = PCALL(dev, recvex, REQ_STATUS, 0, 0, stat, 2);
    if (r == 2)
        return 0;
    else if (r < 0)
        return r;
    else
        return -EIO;
}

static int getkey(struct AvrkDev* dev, void *buf)
{
    int r = PCALL(dev, recvex, REQ_CHANGE_KEY, 0, 0, buf, 16);
    if (r == 16)
        return 0;
    else if (r < 0)
        return r;
    else
        return -EIO;
}

static int putkey(struct AvrkDev* dev, const void *buf)
{
    int r = PCALL(dev, sendex, REQ_CHANGE_KEY, 0, 0, buf, 16);
    if (r == 16)
        return 0;
    else if (r < 0)
        return r;
    else
        return -EIO;
}

static struct AvrkDev* from_inode(struct inode* inode)
{
    if (USB_MAJOR == imajor(inode)) {
        int subminor = iminor(inode);
        struct usb_interface *intf = usb_find_interface(&avrk_driver, subminor);
        return usb_get_intfdata(intf);
    } else
        return container_of(inode->i_cdev, struct PrivateData, cdev)->self;
}


struct AvrkDevClass AvrkDevClass = {
    .new = new,
    .delete= delete,
    .ref = ref,
    .unref= unref,
    .ledctl = ledctl,
    .stat = stat,
    .send = send,
    .sendex = sendex,
    .recv = recv,
    .recvex = recvex,
    .flashctl = flashctl,
    .getkey = getkey,
    .putkey = putkey,
    .upload = upload,
    .download = download,
    .start = start,
    .from_inode = from_inode,
    .lock = lock,
    .unlock = unlock,
    .get_devno = get_devno,
};
