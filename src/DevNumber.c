#include "../avrk.h"
#include <linux/slab.h>
#define DEV_NUMBER_COUNT 256

static int init(struct DevNumber* dr, size_t size)
{
    mutex_init(&dr->lock);
    dr->class = &DevNumberClass;
    dr->device_vector = kzalloc(size, GFP_KERNEL);
    if (!dr->device_vector)
        return -ENOMEM;
    dr->vector_size = size;

    int r = alloc_chrdev_region(&dr->data, 0, DEV_NUMBER_COUNT, "avrkchar");
    if (r == 0)
        pr_info("allocate dev number %u.%u+%u",
            MAJOR(dr->data), MINOR(dr->data), DEV_NUMBER_COUNT);
    return r;
}

static void finalize(struct DevNumber* dr)
{
    kfree(dr->device_vector);
    dr->vector_size = 0;
    unregister_chrdev_region(dr->data, DEV_NUMBER_COUNT);
}

static int alloc_minor(struct DevNumber* dr)
{
    int i;
    while (mutex_lock_interruptible(&dr->lock)) {}
    for (i=0; i < dr->vector_size; i++) {
        if (! dr->device_vector[i])
            break;
    }
    if (i == dr->vector_size)
        i = -1;
    else
        dr->device_vector[i] = 1;
    mutex_unlock(&dr->lock);
    return i;
}

static dev_t mkdev(struct DevNumber* dr, int minor)
{
    return MKDEV(MAJOR(dr->data), minor);
}

static void free_minor(struct DevNumber *dr, int i)
{
    while (mutex_lock_interruptible(&dr->lock)) {}
    dr->device_vector[i] = 0;
    mutex_unlock(&dr->lock);
}

struct DevNumberClass DevNumberClass = {
    .init = init,
    .finalize = finalize,
    .free_minor = free_minor,
    .alloc_minor = alloc_minor,
    .mkdev = mkdev,
};

struct DevNumber DevNumber = {
    .class = &DevNumberClass,
};
