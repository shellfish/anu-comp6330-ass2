#include "../avrk.h"
#include <linux/slab.h>

static struct CryptBuffer *new(struct AvrkDev* dev)
{
    struct CryptBuffer *cb = kmalloc(sizeof(*cb), GFP_KERNEL);
    if (!cb)
        return ERR_PTR(ENOMEM);
    PCALL(dev, ref);
    cb->dev = dev;
    cb->count = 0;
    cb->writed_count = 16;
    cb->class =&CryptBufferClass;
    mutex_init(&cb->lock);
    init_waitqueue_head(&cb->wait);
    return cb;
}

static void delete(struct CryptBuffer* cb)
{
    PCALL(cb->dev, unref);
    kfree(cb);
}

static int upload(struct CryptBuffer* cb)
{
    int r;
    if ((r = PCALL(cb->dev, upload, 0, cb->inbuf)) < 0) return r;
    if ((r = PCALL(cb->dev, upload, 1, cb->inbuf+ 4)) < 0) return r;
    if ((r = PCALL(cb->dev, upload, 2, cb->inbuf+ 8)) < 0) return r;
    if ((r = PCALL(cb->dev, upload, 3, cb->inbuf+ 12)) < 0) return r;
    return 0;
}

static int download(struct CryptBuffer* cb)
{
    int r;
    if ((r = PCALL(cb->dev, download, 0, cb->outbuf)) < 0) return r;
    if ((r = PCALL(cb->dev, download, 1, cb->outbuf+ 8)) < 0) return r;
    return 0;
}

static int encrypt(struct CryptBuffer *cb)
{
    int r;
    if ((r = PCALL(cb->dev, lock)))
        return r;
    r = upload(cb) || PCALL(cb->dev, start);
    if (r)
        goto out;

    uint8_t stat[2];
    while (1) {
       r = PCALL(cb->dev, stat, stat);
       if (r)
           goto out;
       if (!stat[0])
           break;
    }

    if ((r = download(cb)))
        goto out;
    cb->writed_count = 0;
    cb->count = 0;
    r = 0;
out:
    PCALL(cb->dev, unlock);
    return r;
}

static int pull(struct CryptBuffer* cb, uint8_t *outbuf, int size)
{
    int r;
    int wake_up = 0;
    if ((r = mutex_lock_interruptible(&cb->lock)))
        return r;
    if (cb->writed_count == 16) {
        if (cb->count == 16 && (!(r = encrypt(cb)))) {
            wake_up = 1;
        } else {
            r = -EAGAIN;
            goto out;
        }
    }

    if (size > 16 - cb->writed_count)
        size = 16 - cb->writed_count;
    memcpy(outbuf, cb->outbuf + cb->writed_count, size);
    /*pr_info("successfully pull %d bytes", size);*/
    cb->writed_count += size;
    mutex_unlock(&cb->lock);
    wake_up_interruptible(&cb->wait);
    return size;
out:
    mutex_unlock(&cb->lock);
    if (wake_up)
        wake_up_interruptible(&cb->wait);
    return r;
}

static int push(struct CryptBuffer *cb, uint8_t *inbuf, int size)
{
    int r, wake_up = 0;
    if (size <= 0)
        return 0;
    if (size > 16)
        size = 16;
    if ((r = (mutex_lock_interruptible(&cb->lock))))
        return r;

    if (cb->count == 16) {
        if (cb->writed_count == 16 && (!(encrypt(cb)))) {
            wake_up = 1;
        } else {
            r = -EAGAIN;
            goto out;
        }
    }

    size = min(16 - cb->count, size);
    memcpy(cb->inbuf + cb->count, inbuf, size);

    /*uint8_t *print_buf = kzalloc(size+1, GFP_KERNEL);*/
    /*memcpy(print_buf, cb->inbuf + cb->count, size);*/
    /*pr_info("successfully push %d bytes: %s\n", size, print_buf);*/

    cb->count += size;
    r = size;
out:
    mutex_unlock(&cb->lock);
    if (wake_up)
        wake_up_interruptible(&cb->wait);
    return r;
}

struct CryptBufferClass CryptBufferClass = {
    .new = new,
    .delete = delete,
    .push = push,
    .pull = pull,
};

