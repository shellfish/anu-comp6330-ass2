#include "../avrk.h"
#include <linux/version.h>

static ssize_t show_flash(struct device *device, struct device_attribute *attr, char *buf)
{
    uint8_t stat[2];
    struct AvrkDev *dev = (struct AvrkDev*)dev_get_drvdata(device);
    int r = PCALL(dev, stat, stat);
    if (r < 0)
        return r;
    else
        return scnprintf(buf, 3, "%c\n", stat[1] ? '1' : '0');
}

static ssize_t show_stat(struct device *device, struct device_attribute *attr, char *buf)
{
    uint8_t stat[2];
    struct AvrkDev *dev = (struct AvrkDev*)dev_get_drvdata(device);
    int r = PCALL(dev, stat, stat);
    if (r < 0)
        return r;
    else
        return scnprintf(buf, 256, STAT_FORMAT, stat[0], stat[1]);
}

static ssize_t show_key(struct device *device, struct device_attribute *attr, char *buf)
{
    uint8_t key[16];
    struct AvrkDev *dev = (struct AvrkDev*)dev_get_drvdata(device);
    int r = PCALL(dev, getkey, key);
    if (r == 0) {
        memcpy(buf, key, 16);
        return 16;
    } else
        return r;
}

static ssize_t store_key(struct device *device, struct device_attribute *attr,
                           const char *buf, size_t count)
{
    struct AvrkDev *dev = (struct AvrkDev*)dev_get_drvdata(device);
    if (count >= 16) {
        int r = PCALL(dev, putkey, buf);
        return r == 0 ? count : r;
    } else
        return -EINVAL;
}


static ssize_t store_flash(struct device *device, struct device_attribute *attr,
                           const char *buf, size_t count)
{
    struct AvrkDev *dev = (struct AvrkDev*)dev_get_drvdata(device);
    if (count >= 1) {
        int r = PCALL(dev, flashctl, !(buf[0] == '0'));
        return r == 0 ? count : r;
    } else
        return -EINVAL;
}

static ssize_t store_led(struct device *device, struct device_attribute *attr,
                           const char *buf, size_t count)
{
    struct AvrkDev *dev = (struct AvrkDev*)dev_get_drvdata(device);
    if (count >= 1) {
        int r = PCALL(dev, ledctl, !(buf[0] == '0'));
        return r == 0 ? count : r;
    } else
        return -EINVAL;
}


static DEVICE_ATTR(flash, 0660, show_flash, store_flash);
static DEVICE_ATTR(led, 0220, NULL, store_led);
static DEVICE_ATTR(key, 0660, show_key, store_key);
static DEVICE_ATTR(stat, 0440, show_stat, NULL);

static struct device_attribute *device_attributes[] = {
    &dev_attr_flash,
    &dev_attr_led,
    &dev_attr_key,
    &dev_attr_stat,
    NULL,
};
const struct device_attribute **dev_attributes =
    (const struct device_attribute**)device_attributes;
