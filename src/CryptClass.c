#include "../avrk.h"

static char *input_devnode(struct device *dev, umode_t *mode)
{
    // this is the name show under devtmpfs, ie, /dvv/crypt/%s
    return kasprintf(GFP_KERNEL, "crypt/%s", dev_name(dev));
}

static void ref_crypt_class(struct CryptClass *c)
{
    kref_get(&c->ref);
}

static void crypt_class_finalizer(struct kref *ref)
{
    pr_info("unregister class /crypt\n");
    class_unregister(&container_of(ref, struct CryptClass, ref)->data);
}

static void unref_crypt_class(struct CryptClass *c)
{
    kref_put(&c->ref, crypt_class_finalizer);
}

static int init_crypt_class(struct CryptClass *c)
{
    int r = class_register(&CryptClass.data);
    if (r == 0)
        kref_init(&c->ref);
    return r;
}

static struct class *get(struct CryptClass*c)
{
    return &c->data;
}

struct CryptClass CryptClass = {
    .class = &CryptClassClass,
    .data = {
        // device show show up under /dev/crupt
        .name = "crypt",
        .devnode = input_devnode,
    },
};


struct CryptClassClass CryptClassClass = {
    .init = init_crypt_class,
    .ref = ref_crypt_class,
    .unref = unref_crypt_class,
    .get = get,
};
