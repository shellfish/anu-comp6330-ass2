#!/bin/bash

EXE=avr
MAXLEN=4096
LOOPS=100

while [[ $# -gt 0 ]]; do
    eval "$1"
    shift
done

TMPFILE=`mktemp`
trap "rm  $TMPFILE" EXIT
# libusb is not reentrant, a pipeline with two invokes to $EXE wouldn't work
./$EXE -e < /etc/fstab > $TMPFILE || (echo "encryption of /etc/fstab not work"; exit 1)
if ! diff /etc/fstab <((./$EXE -r < $TMPFILE)); then
    exit 1
fi

count=0

function generate_data {
    dd if=/dev/urandom bs=1 count=$1 2>/dev/null
}

KEY=`./$EXE -d`
datafile=`mktemp`

printf "key: $KEY\n"
printf "max len: $MAXLEN\n"

while ((count++ < $LOOPS)); do
    len=$((RANDOM % $MAXLEN))
    printf "%-10s len: %-10d" "$count/$LOOPS" $len
    generate_data $len > $datafile
    cmp <(cat $datafile | ./$EXE -e)\
        <(cat $datafile | openssl enc -aes-128-ecb -K $KEY -iv $KEY)
    if (($? == 0)); then
        echo ' ok'
    else
        echo " failed"
        exit 1
    fi
done
rm $datafile
