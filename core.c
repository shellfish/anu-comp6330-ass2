#include "avrk.h"
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

/* table of devices that work with this driver */
static struct usb_device_id avrk_devid_table [] = {
	{USB_DEVICE(AVR_VENDOR_ID, AVR_PRODUCT_ID)},
	{}					/* Terminating entry */
};
MODULE_DEVICE_TABLE(usb, avrk_devid_table);

static int __init avrk_init(void)
{
	int r = CALL(CryptClass, init);
    if (r) {
        pr_err("unable to register crypt_dev class\n");
        return r;
    }

    r = CALL(DevNumber, init, 64);
    if (r) {
        pr_err("failed to allocate dev number");
        goto err1;
    }

	if ((r = usb_register(&avrk_driver)) != 0) {
		pr_err("avrkusb: usb_register failed. Error number %d", r);
        goto err2;
    } else
        return 0;
err2:
    CALL(DevNumber, finalize);
err1:
	CALL(CryptClass, unref);
    return r;
}

static void __exit avrk_exit(void)
{
    CALL(CryptClass, unref);
	usb_deregister(&avrk_driver);
    CALL(DevNumber, finalize);
}


static int avrk_open(struct inode *inode, struct file *file)
{
    struct AvrkDev *dev =  AvrkDevClass.from_inode(inode);
    struct CryptBuffer *cb = CryptBufferClass.new(dev);
    if (IS_ERR(cb))
        return PTR_ERR(cb);
    file->private_data = cb;
    return 0;
}

static int avrk_release(struct inode *inode, struct file *file)
{
    struct CryptBuffer *cb = (struct CryptBuffer*) file->private_data;
    PCALL(cb, delete);
    return 0;
}


static ssize_t avrk_read(struct file *file, char __user *outbuf, size_t count, loff_t *off)
{
    struct CryptBuffer *cb= (struct CryptBuffer*)file->private_data;
    uint8_t buf[16];
    int r = PCALL(cb, pull, buf, count);
    while (r  == -EAGAIN) {
        wait_event_interruptible(cb->wait, cb->writed_count < 16);
        r = PCALL(cb, pull, buf, count);
    }
    if (r > 0)
        if (copy_to_user (outbuf, buf, r))
            return -EFAULT;
    return r;
}

static ssize_t avrk_write(struct file *file, const char __user *inbuf, size_t count, loff_t *off)
{
    struct CryptBuffer *cb= (struct CryptBuffer*)file->private_data;
    if (count > 16)
        count = 16;
    uint8_t buf[16];
    if (copy_from_user (buf, inbuf, count))
        return -EFAULT;
    int r = -EAGAIN;
    while (r == -EAGAIN) {
        wait_event_interruptible(cb->wait, cb->count < 16);
        r = PCALL(cb, push, buf, count);
    }
    return r;
}

struct file_operations avrk_fops = {
	.read =		avrk_read,
	.write =    avrk_write,
	.open =		avrk_open,
	.release =	avrk_release,
};

struct usb_class_driver avrk_class = {
	.name = "avrkusb%d",
	.fops = &avrk_fops,
	.minor_base = USB_MINOR_BASE,
};

static int avrk_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
    struct AvrkDev *dev = AvrkDevClass.new(interface);
    return IS_ERR(dev) ? PTR_ERR(dev) : 0;
}

static void avrk_disconnect(struct usb_interface *interface)
{
    struct AvrkDev* dev =  usb_get_intfdata(interface);
	dev_info(&interface->dev,
        "Avrk crypt stick #%d now disconnected", MINOR(PCALL(dev, get_devno)));

    PCALL(dev, delete);
}

struct usb_driver avrk_driver = {
	.name = "avrkusb",
	.id_table = avrk_devid_table,
	.probe = avrk_probe,
	.disconnect = avrk_disconnect,
};

module_init (avrk_init);
module_exit (avrk_exit);

MODULE_DESCRIPTION("anu/avr usb crypt stick driver");
MODULE_LICENSE("GPL");
