CC=gcc -std=c99
LDFLAGS=
CFLAGS=  -Wall -O0 -g

all: avr avrk
avr: avr.o avr_user.o
	$(CC) $(LDFLAGS)  $^ -lusb-1.0 -o $@
avrk: avr.o avr_kern.o
	$(CC) $(LDFLAGS)  $^  -o $@
avr_user.o: avr_user.c avr.h
avr_kern.o: avr_kern.c avr.h
avr.o: avr.c avr.h

test:all
	sudo modprobe avrk
	./test.sh EXE=avrk MAXLEN=129 LOOPS=10
	sudo rmmod  avrk
	./test.sh EXE=avr MAXLEN=129 LOOPS=10
clean:
	rm -f avr avrk *.o
cleanall: clean
	git clean -x -f
.PHONY: clean test
