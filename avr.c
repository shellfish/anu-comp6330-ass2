#define AVR_IMPL
#include "avr.h"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#define SHORT_OPT_MAXLEN 255

typedef int (*action_t)(void);
extern __attribute__((noreturn)) void cleanup_and_exit();

struct action {
    action_t act;
    int nousb;
    const char *info;
    const char *name;
    int has_arg;
};

struct DevStat {
    uint8_t progress;
    uint8_t led;
};

int global_retval;

struct globals {
    int retval;
    const char *progname;
    struct option *long_options;
    char short_options[SHORT_OPT_MAXLEN];
    struct action *actions;
};
struct globals G = {
    .retval = EXIT_FAILURE
};

char device_index[32] = "0";

static int action_index()
{
    strcpy(device_index, optarg);
    return 0;
}

static int usage()
{
    struct action *ap;
    printf( "Usage: %s [OPTION]... \n  control avr usb stick\n", G.progname);
    for (int i = 0; i < 256; i++) {
        ap = G.actions + i;
        if (ap->act) {
            printf("  -%c, --%-10s%s\n", i, ap->name, ap->info);
        }
    }
    return EXIT_SUCCESS;
}

int action_decrypt()
{
    char buf[128];
    snprintf(buf, sizeof(buf),
        "openssl enc -d -aes-128-ecb -K `%s -d` -iv `%s -d`",
        G.progname, G.progname);
    return system(buf);
}

int action_upload_key()
{
    unsigned char hexbuf[16];
    char convbuf[3] = {0};
    if (optarg && strlen(optarg) == 32) {
        int i = 0;
        do {
            memcpy(convbuf, optarg+2*i, 2);
            hexbuf[i] = strtol(convbuf, NULL, 16);
        } while (++i != sizeof(hexbuf));
    } else {
        int count = 0;
        do {
            count += fread(hexbuf+count, 1, sizeof(hexbuf)-count, stdin);
        } while (!(feof(stdin) || ferror(stdin)) && count < sizeof(hexbuf));
        if (count != sizeof(hexbuf)) {
            ERRMSG("no enough key data fed, abort...");
            return EXIT_FAILURE;
        }
    }
    return write_key(hexbuf);
}

int reset_key()
{
    static unsigned char default_key[] = "OperatingSystems";
    return write_key(default_key);
}


int action_encrypt()
{
    extern void encrypt_and_recv_block(unsigned char (*buf)[16]);
    unsigned count = 0;
    unsigned char buf[16];
    while (1 == fread(buf + count, 1, 1, stdin)) {
        count += 1;
        if (count == 16) {
            encrypt_and_recv_block(&buf);
            fwrite(buf, 16, 1, stdout);
        }
        count &= 0x0f;
    }

    unsigned char padding = 16 - count;
    memset(buf + (16 - padding), padding, padding);
    encrypt_and_recv_block(&buf);
    fwrite(buf, 1, 16, stdout);

    return EXIT_SUCCESS;
}


struct action global_actions[255] = {
    ['s'] = {
        .act = action_status,
        .info = "report encryption progress and led state",
        .name = "status",
    },
    ['o'] = {
        .act = action_led_on,
        .info = "turn on white led",
        .name = "on",
    },
    ['O'] = {
        .act = action_led_off,
        .info = "turn off white led",
        .name = "off",
    },
    ['h'] = {
        .act = usage,
        .nousb = 1,
        .info = "display help message",
        .name = "help",
    },
    ['f'] = {
        .act = action_led_flush_on,
        .info = "enable led flushing",
        .name = "flush",
    },
    ['F'] = {
        .act = action_led_flush_off,
        .info = "stop led flushing",
        .name = "noflush",
    },
    ['e'] = {
        .act = action_encrypt,
        .info = "encrypt data",
        .name = "encrypt",
    },
    ['d'] = {
        .act = action_download_key,
        .info = "download encryption key",
        .name = "download",
    },
    ['u'] = {
        .act = action_upload_key,
        .info = "upload encryption key",
        .name = "upload",
        .has_arg = optional_argument,
    },
    ['r'] = {
        .act = action_decrypt,
        .info = "decrypt data",
        .name = "decrypt",
        .nousb = 1,
    },
    ['i'] = {
        .act = action_index,
        .info = "specifiy device index",
        .name = "index",
        .has_arg = required_argument,
    },
    ['R'] = {
        .act = reset_key,
        .info = "reset default key",
        .name = "reset",
    }
};

void construct_options()
{
    int count = 1;
    struct action *ap;
    char *sop = G.short_options;
    for (int i = 0; i < 256; i++) {
        ap = G.actions + i;
        if (ap->act) {
            count += 1;
            *sop++ = i;
            if (ap->has_arg == optional_argument) {
                *sop++ = ':';
                *sop++ = ':';
            } else if (ap->has_arg == required_argument) {
                *sop++ = ':';
            }
        }
    }
    G.long_options = (struct option*)malloc(sizeof(struct option) * count);
    if (!G.long_options) {
        ERRMSG("malloc failed");
        exit(EXIT_FAILURE);
    }
    struct option *loptp = G.long_options;
    for (int i=0; i < 256; i++) {
        ap = G.actions + i;
        if (ap->act) {
            loptp->name = ap->name;
            loptp->has_arg = ap->has_arg;
            loptp->flag = NULL;
            loptp->val = i;
            loptp++;
        }
    }
    memset(loptp, 0, sizeof(*loptp));
}

int main(int argc, char **argv)
{
    G.progname = argv[0];
    G.actions = global_actions;

    if (argc == 1) {
        usage();
        G.retval = EXIT_SUCCESS;
        goto out;
    }

    int opt;
    construct_options();
    while ((opt = getopt_long(argc, argv, G.short_options, G.long_options, NULL)) != -1) {
        if (G.actions[opt].act) {
            setup_usb(G.actions[opt].nousb);
            G.retval = G.actions[opt].act();
        }
    }
out:
    cleanup_and_exit();
}
