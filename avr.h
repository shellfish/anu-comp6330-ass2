#ifndef AVR_H
#define AVR_H
#ifndef __KERNEL__
#include <stdint.h>
#else
#include <linux/types.h>
#endif
#include <linux/ioctl.h>

#define AVR_VENDOR_ID 0xf055
#define AVR_PRODUCT_ID 0xcec5
#define STAT_FORMAT "progress\t%x\nled-stat\t%x\n"

enum Request {
    REQ_ECHO,
    REQ_STATUS,
    REQ_LED_ON,
    REQ_LED_OFF,
    REQ_UPLOAD_A,
    REQ_UPLOAD_B,
    REQ_UPLOAD_C,
    REQ_UPLOAD_D,
    REQ_DOWNLOAD_A,
    REQ_DOWNLOAD_B,
    REQ_START_ENCRYPT,
    REQ_LED_CTL,
    REQ_CHANGE_KEY,
};

#ifdef AVR_IMPL
#define ERRMSG(msg) do { fprintf(stderr, "%s:%d:%s(): %s\n", __FILE__, __LINE__, __func__, msg); } while (0)
extern int action_status();
extern int action_download_key();
extern int action_led_on();
extern int action_led_off();
extern int action_led_flush_on();
extern int action_led_flush_off();
extern void encrypt_and_recv_block(unsigned char (*buf)[16]);
int write_key(unsigned char *hexbuf);
extern int global_retval;
void setup_usb(int nousb);
extern char device_index[32];
#endif

#endif
