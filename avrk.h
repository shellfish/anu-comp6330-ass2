#ifndef _AVRK_H_
#define _AVRK_H_

#include "avr.h"
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/kref.h>
#include <linux/usb.h>
#include <linux/mutex.h>
#include <linux/wait.h>

#define USB_MINOR_BASE 192

#define CALL(obj, method, ...) ((obj).class->method(&obj, ##__VA_ARGS__))
#define PCALL(ptr, method, ...) ((ptr)->class->method(ptr, ##__VA_ARGS__))
#define GET(obj, ...) CALL(obj, get, ##__VA_ARGS__)

extern struct usb_driver avrk_driver;
extern struct file_operations avrk_fops;
extern struct usb_class_driver avrk_class;

struct AvrkDevClass;
struct AvrkDev {
    struct usb_device *udev;
    struct mutex lock;
    struct kref kref;
    void *private_data;
    struct AvrkDevClass *class;
};

struct AvrkDevClass {
    struct AvrkDev *(*new)(struct usb_interface *intf);
    void (*unref)(struct AvrkDev*);
    void (*ref)(struct AvrkDev*);
    void (*delete)(struct AvrkDev*);
    int (*lock)(struct AvrkDev*);
    void (*unlock)(struct AvrkDev*);
    int (*ledctl)(struct AvrkDev*, int to);
    int (*flashctl)(struct AvrkDev*, int to);
    int (*stat)(struct AvrkDev*, char *stat);
    int (*sendex)(struct AvrkDev*, uint8_t request, uint16_t value, uint16_t index,
                                              const void *buf, uint16_t size);
    int (*send)(struct AvrkDev*, uint8_t request);
    int (*recvex)(struct AvrkDev*, uint8_t , uint16_t , uint16_t , void *, uint16_t);
    int (*recv)(struct AvrkDev*, uint8_t request);
    int (*getkey)(struct AvrkDev* dev, void *buf);
    int (*putkey)(struct AvrkDev* dev, const void*buf);
    int (*upload)(struct AvrkDev* dev, int idx, uint8_t *block4b);
    int (*download)(struct AvrkDev* dev, int idx, uint8_t *block8b);
    int (*start)(struct AvrkDev* dev);
    dev_t (*get_devno)(struct AvrkDev* dev);

    struct AvrkDev* (*from_inode)(struct inode* inode);
};
extern struct AvrkDevClass AvrkDevClass;

struct DevNumberClass;
struct DevNumber {
    dev_t data;
    struct mutex lock;
    unsigned char *device_vector;
    size_t vector_size;
    struct DevNumberClass *class;
};
struct DevNumberClass {
    int (*init)(struct DevNumber*, size_t size);
    void (*finalize)(struct DevNumber*);
    int (*alloc_minor)(struct DevNumber*);
    void (*free_minor)(struct DevNumber*, int minor);
    dev_t (*mkdev)(struct DevNumber*, int minor);
};
extern struct DevNumberClass DevNumberClass;
extern struct DevNumber DevNumber;

struct CryptClassClass;
struct CryptClass {
    struct class data;
    struct kref ref;
    struct CryptClassClass *class;
};
struct CryptClassClass {
    int (*init)(struct CryptClass*);
    void (*ref)(struct CryptClass*);
    void (*unref)(struct CryptClass*);
    struct class *(*get)(struct CryptClass*);
};
extern struct CryptClassClass CryptClassClass;
extern struct CryptClass CryptClass;

struct CryptBufferClass;
struct CryptBuffer {
    struct AvrkDev* dev;
    uint8_t inbuf[16];
    uint8_t outbuf[16];
    int    count;
    struct mutex lock;
    int    writed_count; // 16 : full, 0 : no data
    wait_queue_head_t wait;
    struct CryptBufferClass *class;
};
struct CryptBufferClass {
    struct CryptBuffer *(*new)(struct AvrkDev*);
    void (*delete)(struct CryptBuffer*);
    int (*push)(struct CryptBuffer*, uint8_t *inbuf, int size);
    int (*pull)(struct CryptBuffer* cb, uint8_t *outbuf, int size);
};
extern struct CryptBufferClass CryptBufferClass;

#endif
